
let switchMode = document.getElementById("switchMode");

switchMode.onclick = function ()
{
    let theme = document.getElementById("theme");

    if (theme.getAttribute("href") === "HW14_light.css")
    {
        theme.href ="HW14_dark.css"
    }
    else
    {
        theme.href ="HW14_light.css"
    }
}