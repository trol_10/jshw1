//1)Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let myParagraph = document.getElementsByTagName('p')
for (let i = 0; i<myParagraph.length; i++ ){
    myParagraph[i].style.backgroundColor ='#ff0000';
}
//2)Знайти елемент із id="optionsList". Вивести у консоль.
// Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const elementId = document.getElementById('optionsList');
const selectorId = document.querySelector("#optionsList");


console.log(elementId);
console.log(elementId.parentElement);
console.log(elementId.children);

for (let i = 0; i<elementId.children.length; i++){
    console.log(elementId.children[i].nodeName)
    console.log(elementId.children[i].nodeType)
}

//3)Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

let testParagraph = document.getElementById('testParagraph')
console.log(testParagraph)

testParagraph.textContent = 'This is paragraph'
console.log(testParagraph)

//4)Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
let mainHeaderElems = document.querySelector('.main-header')
console.log(mainHeaderElems)

for (let i = 0; i <mainHeaderElems.children.length; i++){
    mainHeaderElems.children[i].className='nav-item'
}

//6)Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

// let sectionsTitle = document.querySelectorAll('.section-title');
// for (let i = 0; i <sectionsTitle.children.length; i++){
//     sectionsTitle[i].remove('section-title');
//     // sectionsTitle[i].className=''
// }


//ТЕОРІЯ
//1)Опишіть своїми словами що таке Document Object Model (DOM)- це об'єктна модель документа, яка представляє собою
//вміст сторінки у вигляді об'єктів.
//2)Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText витягує та встановлює вміст тега у вигляді простого тексту, тоді як innerHTML витягує та встановлює вміст у форматі HTML.
//3)Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//За допомогою елементів таких як getElementById(),getElementsByClassName(), getElementsByTagName(). Але найкраще буде звернутися
//за допомогою querySelector або querySelectorAll.