


function showPassword (iconId,inputId) {
    let input = document.getElementById(inputId);
    let icon = document.getElementById(iconId)
    if (input.type === "password" ) {
        icon.className= "fas fa-eye-slash icon-password"
        input.type = "text";
    } else {
        input.type = "password";
        icon.className= "fas fa-eye icon-password"
    }
}



function validButton () {
    let firstInput = document.getElementById('firstPassword')
    let secondInput = document.getElementById('secondPassword')
    let errorMessage = document.getElementById('error')
    if (firstInput.value === secondInput.value) {
        errorMessage.innerText = ''
        alert('You are welcome')
    } else {
        errorMessage.innerText = "Потрібно ввести однакові значення"
        errorMessage.style.color = 'red'
    }
}
